import javafx.geometry.Insets;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class HelpPane extends VBox {
    public HelpPane() {
        Text help1 = new Text("1. Start the game with pressing \"START\" button.");
        Text help2 = new Text("2. Read the quotation and decide whether you like it, or not.");
        Text help3 = new Text("3. If you like it, press \"LIKE\" button, if not, press \"DISLIKE\" button.");
        Text help4 = new Text("4. After you like/dislike the quotation, the app is going to give you another one.");
        Text help5 = new Text("5. After each like/dislike, the statistics on the bottom left corner will change.");
        Text help6 = new Text("6. Each author in statistics has a rating, which depends on whether you like, or dislike his quotations. Rating 0 is the worst, 1 is the best.");
        Text help7 = new Text("7. You can go on however long you want. However, if you press the \"STOP\" button, or the app runs out of quotations, then the app will end.");
        Text help8 = new Text("8. After the app ends, it will show you your most liked author, and a global score (all likes divided by number of quotations shown from all authors. ");

        setPadding(new Insets(10, 10, 10, 10));
        setSpacing(10);
//        setAlignment(Pos.CENTER_LEFT);
        getChildren().addAll(help1, help2, help3, help4, help5, help6, help7, help8);
    }
}
