import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

public class ShowPane extends VBox implements IObserver {
    private Text authorName = new Text("");
    private Text quotation = new Text("");
    private App app;

    public ShowPane(App app) {
        this.app = app;
        app.register(this);

        authorName.setStyle("-fx-font: 24 arial;");
        authorName.setWrappingWidth(400);
        authorName.setTextAlignment(TextAlignment.LEFT);
        quotation.setWrappingWidth(400);
        quotation.setTextAlignment(TextAlignment.LEFT);

        update();

        getChildren().addAll(authorName, quotation);
        setStyle("-fx-background-color: #74b9ff");
        setSpacing(20);
        setPadding(new Insets(120, 0, 0, 260));
        setAlignment(Pos.TOP_LEFT);
    }

    @Override
    public void update() {
        if (app.isAppStarted()) {
            authorName.setText(app.getCurrentAuthor().getName());
            quotation.setText(app.getCurrentQuatation().toString());
        }
        if (app.isShowEndStatistics()) {
            String name = app.getBestScoreAuthor().getName();
            if (Author.globalQuotationsRead <= 0) {
                name = "-";
            }
            authorName.setText("Best author: " + name);
            quotation.setText("Global score: " + Author.globalScore);
            quotation.setStyle("-fx-font: 24 arial;");
        }
    }
}
