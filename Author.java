import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Author {
    public static double globalScore;
    public static ArrayList<Double> globalScoreHistory = new ArrayList<>();
    public static int globalQuotationsRead;
    public static int globalQuotationsLiked;

    private int ID;
    private String name;
    private double score = 0.0;
    private ArrayList<Quotation> quotations = new ArrayList<>();
    private int nextQuotationIndex = -1;
    private int quotationsRead = 0;
    private int quotationsLiked = 0;

    public Author(int ID, String name, String quotationsFile) {
        this.ID = ID;
        this.name = name;

        fetchQuotations(quotationsFile);

        if (!quotations.isEmpty()) {
            nextQuotationIndex = 0;
        }
    }

    public void handleReadQuotation(boolean quotationLiked) {
        quotationsRead++;
        globalQuotationsRead++;
        if (quotationLiked) {
            quotationsLiked++;
            globalQuotationsLiked++;
        }

        score = Utils.roundToTwoDecimals((double) quotationsLiked / quotationsRead);
        globalScore = Utils.roundToTwoDecimals((double) globalQuotationsLiked / globalQuotationsRead);

        globalScoreHistory.add(globalScore);
    }

    public int getNextQuotationIndex() {
        return nextQuotationIndex;
    }

    public void setNextQuotationIndex(int nextQuotationIndex) {
        if (nextQuotationIndex < quotations.size()) {
            this.nextQuotationIndex = nextQuotationIndex;
        } else {
            this.nextQuotationIndex = -1;
        }
    }

    public void shuffleQuotations() {
        Collections.shuffle(quotations);
    }

    public double getScore() {
        return score;
    }

    public String getName() {
        return name;
    }

    public void fetchQuotations(String quotationsFile) {
        try {
            Scanner scan = new Scanner(new File(quotationsFile));
            int i = 0;
            while (scan.hasNextLine()) {
                String text = scan.nextLine();
                if (!text.isEmpty()) {
                    Quotation quotation = new Quotation(i, text);
                    quotations.add(quotation);
                }
                i++;
            }
        } catch (Exception e) {
            System.out.println("There was an error");
        }
    }

    public ArrayList<Quotation> getQuotations() {
        return quotations;
    }

    public int getID() {
        return ID;
    }
}
