import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

public class AppPane extends VBox implements IObserver {
    private App app;

    public AppPane() {
        app = new App();
        app.register(this);

        ShowPane showPane = new ShowPane(app);
        ControlsPane controlsPane = new ControlsPane(app);

        setVgrow(showPane, Priority.ALWAYS);

        getChildren().addAll(showPane, controlsPane);
    }

    @Override
    public void update() {

    }
}
