Change is the law of life. And those who look only to the past or present are certain to miss the future.
Democracy and defense are not substitutes for one another. Either alone will fail.
Forgive your enemies, but never forget their names.
I would rather be accused of breaking precedents than breaking promises.
If we cannot end now our differences, at least we can help make the world safe for diversity.
Let us never negotiate out of fear but let us never fear to negotiate.
Liberty without learning is always in peril; learning without liberty is always in vain.
The ancient Greek definition of happiness was the full use of your powers along lines of excellence.
The time to repair the roof is when the sun is shining.
Washington is a city of Southern efficiency and Northern charm.
We have come too far, we have sacrificed too much, to disdain the future now.
We must use time as a tool, not as a crutch.
We set sail on this new sea because there is knowledge to be gained.
We stand for freedom. That is our conviction for ourselves; that is our only commitment to others.
If a free society cannot help the many who are poor, it cannot save the few who are rich.