import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Start extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        AppPane pane = new AppPane();
        Scene scene = new Scene(pane, 900, 480);

        primaryStage.setTitle("Quotations selector");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
