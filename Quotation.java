public class Quotation {
    private int ID;
    private String text;

    public Quotation(int ID, String text) {
        this.ID = ID;
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
