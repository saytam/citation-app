public class Utils {
    public static double roundToTwoDecimals(double number) {
        return Math.round(number * 100) / 100d;
    }
}
