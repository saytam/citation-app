public interface ISubject {
    /**
     * @param observer
     */
    void register(IObserver observer);


    /**
     * @param observer
     */
    void unregister(IObserver observer);

    void notifyObservers();
}
