import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class App implements ISubject {
    ArrayList<Author> authors = new ArrayList<>();
    ArrayList<Author> authorsWithQuotations = new ArrayList<>();
    boolean appStarted = false;
    boolean showEndStatistics = false;
    Random generator = new Random();
    private Set<IObserver> observerList;
    private Author currentAuthor = null;
    private Quotation currentQuatation = null;

    public App() {
        observerList = new HashSet<>();

        Author johnFKennedy = new Author(0, "John F. Kennedy", "johnfkennedy.txt");
        Author socrates = new Author(1, "Socrates", "socrates.txt");
        Author steveJobs = new Author(2, "Steve Jobs", "stevejobs.txt");
        Author terryPratchett = new Author(3, "Terry Pratchett", "terrypratchett.txt");
        Author williamShakespeare = new Author(4, "William Shakespeare", "williamshakespeare.txt");

        authors.add(johnFKennedy);
        authors.add(socrates);
        authors.add(steveJobs);
        authors.add(terryPratchett);
        authors.add(williamShakespeare);

        for (Author author : authors) {
            if (author.getNextQuotationIndex() >= 0) {
                author.shuffleQuotations();
                authorsWithQuotations.add(author);
            }
        }
    }

    public Author getBestScoreAuthor() {
        Author bestScoreAuthor = null;
        for (Author author : authors) {
            if (bestScoreAuthor == null || author.getScore() > bestScoreAuthor.getScore()) {
                bestScoreAuthor = author;
            }
        }
        return bestScoreAuthor;
    }

    public Author getCurrentAuthor() {
        return currentAuthor;
    }

    public Quotation getCurrentQuatation() {
        return currentQuatation;
    }

    public boolean isAppStarted() {
        return appStarted;
    }

    public void setAppStarted(boolean appStarted) {
        this.appStarted = appStarted;

        if (isAppStarted()) {
            handleLoadNextQuotation();
        } else {
            showEndStatistics = true;
        }

        notifyObservers();
    }

    public boolean isShowEndStatistics() {
        return showEndStatistics;
    }

    public void handleLike() {
        currentAuthor.handleReadQuotation(true);
        handleLoadNextQuotation();
    }

    public void handleDislike() {
        currentAuthor.handleReadQuotation(false);
        handleLoadNextQuotation();
    }

    public void handleLoadNextQuotation() {
        if (!authorsWithQuotations.isEmpty()) {
            int randomAuthorIndex = generator.nextInt(authorsWithQuotations.size());
            currentAuthor = authorsWithQuotations.get(randomAuthorIndex);

            int nextQuotationIndex = currentAuthor.getNextQuotationIndex();
            currentQuatation = currentAuthor.getQuotations().get(nextQuotationIndex);
            currentAuthor.setNextQuotationIndex(nextQuotationIndex + 1);

            if (currentAuthor.getNextQuotationIndex() < 0) {
                authorsWithQuotations.remove(currentAuthor);
            }
        } else {
            setAppStarted(false);
        }
        notifyObservers();
    }


    public ArrayList<Author> getAuthors() {
        return authors;
    }

    @Override
    public void register(IObserver observer) {
        observerList.add(observer);
    }

    @Override
    public void unregister(IObserver observer) {
        observerList.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for (IObserver observer : observerList) {
            observer.update();
        }
    }
}
