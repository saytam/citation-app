import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class ControlsPane extends HBox implements IObserver {
    App app;
    VBox statistics = new VBox();
    VBox middleControls = new VBox();
    VBox rightControls = new VBox();
    private Button likeBtn = new Button("LIKE");
    private Button dislikeBtn = new Button("DISLIKE");
    private Button helpBtn = new Button("HELP");
    private Button startBtn = new Button("START");
    private Button stopBtn = new Button("STOP");
    private Button startStopBtn = startBtn;
    private Button graphBtn = new Button("GRAPH");

    public ControlsPane(App app) {
        this.app = app;
        app.register(this);
        handleSetupButons();

        handleUpdateStatistics();
        statistics.setAlignment(Pos.CENTER);
        middleControls.getChildren().addAll(likeBtn, dislikeBtn);
        middleControls.setAlignment(Pos.CENTER);
        rightControls.getChildren().addAll(helpBtn, graphBtn, startStopBtn);
        rightControls.setAlignment(Pos.CENTER);

        setHgrow(statistics, Priority.ALWAYS);
        setHgrow(middleControls, Priority.ALWAYS);
        setHgrow(rightControls, Priority.ALWAYS);
        setPrefHeight(150);
        setStyle("-fx-background-color: #b2bec3");
        setAlignment(Pos.CENTER);

        getChildren().addAll(statistics, middleControls, rightControls);
    }

    private void stop(ActionEvent actionEvent) {
        this.app.setAppStarted(false);
    }

    private void start(ActionEvent actionEvent) {
        this.app.setAppStarted(true);
        likeBtn.setDisable(false);
        dislikeBtn.setDisable(false);
        startStopBtn = stopBtn;
        handleResetRightControls();
    }

    private void graph(ActionEvent actionEvent) {
        Stage stage = new Stage();

        stage.setTitle("Graph of global score development");
        stage.setScene(new Scene(Graph.show(), 800, 220));
        stage.show();
    }

    private void help(ActionEvent actionEvent) {
        Stage stage = new Stage();
        HelpPane helpPane = new HelpPane();

        stage.setTitle("Help");
        stage.setScene(new Scene(helpPane, 800, 220));
        stage.show();
    }

    private void dislike(ActionEvent actionEvent) {
        app.handleDislike();
    }

    private void like(ActionEvent actionEvent) {
        app.handleLike();
    }

    private void handleUpdateStatistics() {
        statistics.getChildren().clear();
        for (Author author : app.getAuthors()) {
            Text stat = new Text(author.getName() + ": " + author.getScore());
            statistics.getChildren().add(stat);
        }
    }

    public void handleSetupButons() {
        likeBtn.setPrefWidth(100);
        likeBtn.setDisable(true);
        likeBtn.setOnAction(this::like);

        dislikeBtn.setPrefWidth(100);
        dislikeBtn.setDisable(true);
        dislikeBtn.setOnAction(this::dislike);

        graphBtn.setPrefWidth(100);
        graphBtn.setOnAction(this::graph);

        startBtn.setPrefWidth(100);
        startBtn.setOnAction(this::start);

        stopBtn.setPrefWidth(100);
        stopBtn.setOnAction(this::stop);


        helpBtn.setPrefWidth(100);
        helpBtn.setOnAction(this::help);

    }

    public void handleResetRightControls() {
        rightControls.getChildren().clear();
        rightControls.getChildren().addAll(helpBtn, graphBtn, startStopBtn);
    }

    @Override
    public void update() {
        handleUpdateStatistics();
        if (app.isShowEndStatistics()) {
            startStopBtn = startBtn;
            startBtn.setDisable(true);
            likeBtn.setDisable(true);
            dislikeBtn.setDisable(true);
            handleResetRightControls();
        }

    }
}
