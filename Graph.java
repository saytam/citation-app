import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;

public class Graph {

    public static LineChart<Number, Number> show() {
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();

        xAxis.setLabel("Number of quotations");

        yAxis.setUpperBound(1.0);
        yAxis.setLowerBound(0.0);
        yAxis.setAutoRanging(false);

        final LineChart<Number, Number> lineChart =
                new LineChart<>(xAxis, yAxis);

        XYChart.Series<Number, Number> series = new XYChart.Series<>();
        series.setName("Global Score");

        int i = 1;
        for (Double globalScore : Author.globalScoreHistory) {
            series.getData().add(new XYChart.Data<>(i, globalScore));
            i++;
        }
        lineChart.getData().add(series);
        return lineChart;

    }
}
